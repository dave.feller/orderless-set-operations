;;; orderless-set-operations.el --- this file is not part of GNU Emacs. -*- lexical-binding: t -*-
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Rudimentary completion set operations a la Icicles
;;
;;; Code:

(require 'orderless)
(require 'vertico)
(require 'minibuffer)
(eval-when-compile
  (require 'cl-lib))

(defvar orderless-predicate-dispatcher-overrides nil)

(defvar orderless-predicate-dispatchers nil)

(defvar oso-stack-ov nil)

(defvar-local oso--current-set-predicate nil)

(defvar-local oso--narrow t)

(defvar-local oso--set-stack nil
  "The completion set stack.

Set operations operate on the completion sets in the stack
and the current completion set.")

(defface oso-narrow-indicator-face
  '((t (:inherit isearch)))
  "Face for modeline narrow indicator.")

(defface oso-stack-face
  '((t (:foreground "black")))
  "Face for modeline set stack display.")

(defvar-keymap oso-completion-set-map
  "M-SPC" 'oso-push
  "C-M-SPC" 'oso-unwrap
  ;; "C-SPC" nil
  "C-@" 'oso-reduce
  "C-|" 'oso-reverse
  "C-!" 'oso-complement
  "C-<" 'jump-to-register
  "C->" 'oso-completion-set-to-register
  "<backtab>" 'oso-toggle-narrow
  "C-~" 'oso-swap
  "C-M--" 'oso-difference
  "C-+" 'oso-union
  "C-*" 'oso-intersection
  "C-_" 'oso-symmetric-difference
  "C-^" 'oso-unwrap
  "C-#" 'oso-filter)

(cl-defstruct (oso-set)
  operation operands description)

(defun oso--build-pattern-predicate (string table pred complement)
  "Build a predicate for `completing-read' from STRING using
`orderless-pattern-compiler'. If COMPLEMENT is non-nil do negative matching
of the resulting orderless pattern.

TABLE and PRED are `minibuffer-completion-table' and `minibuffer-completion-predicate'."
  (pcase-let* ((`(,_prefix . ,pattern)
                (orderless--prefix+pattern string table pred))
               (`(,regexps . ,preds)
                (orderless-pattern-compiler pattern))
               (case-fold-search-p
                (if orderless-smart-case
                    (cl-loop for regexp in regexps
                             always (isearch-no-upper-case-p regexp t))
                  completion-ignore-case)))
    (make-oso-set
     :operation (if complement
                    (lambda (str)
                      (and (cl-loop with case-fold-search = case-fold-search-p
                                    for re in regexps
                                    thereis (not (string-match-p re str)))
                           (cl-every (lambda (fn) (funcall fn str)) preds)))  
                  (lambda (str)
                    (and (cl-loop with case-fold-search = case-fold-search-p
                                  for re in regexps
                                  always (string-match-p re str))
                         (cl-every (lambda (fn) (funcall fn str)) preds))))
     :operands string
     :description (if complement
                      (concat "(∁ \"" string "\")")
                    (concat "\"" string "\"")))))

(defun oso--build-predicate (predicate _t _p complement)
  "Build a predicate for `completing-read' from STRING using
`orderless-pattern-compiler'. If COMPLEMENT is non-nil do negative matching
of the resulting orderless pattern.

TABLE and PRED are `minibuffer-completion-table' and `minibuffer-completion-predicate'."
  (make-oso-set
   :operation (if complement
                  (lambda (str) (not (funcall (cdr predicate) str)))  
                (cdr predicate))
   :operands nil
   :description (if complement
                    (concat "(∁ " (car predicate) ")")
                  (car predicate))))

(cl-defmethod register-val-describe ((val oso-set) _arg)
  (princ (format "Completion set:\n%s" (oso-set-description val))))

(cl-defmethod register-val-jump-to ((val oso-set) _arg)
  (push val oso--set-stack)
  (oso--update-stack))

(defun oso-completion-set-to-register (register)
  "Save current completion set to register REGISTER."
  (interactive
   (list (register-read-with-preview "Completion Set to register: ")))
  (if oso--set-stack
      (set-register register (car oso--set-stack))
    (user-error "Too few elements in stack")))

(defun oso--clear-stack ()
  "Clear the completion set stack."
  (setq oso--set-stack nil)
  (oso--update-stack))

(defun oso-vertico-buffer--recount ()
    (unless (= vertico-count
               (setq-local vertico-count
                           (- (/ (window-pixel-height
                                  (overlay-get vertico--candidates-ov 'window))
                                 (default-line-height))
                              2
                              (with-temp-buffer
                                (insert (if oso-stack-ov
                                            (overlay-get oso-stack-ov 'before-string)
                                          ""))
                                (count-lines (point-min) (point-max))))))
      (let ((buffer-undo-list t))
        (vertico--display-candidates (vertico--arrange-candidates)))))

(defun oso--setup (fn &rest _args)
  (funcall fn)
  (add-hook 'window-configuration-change-hook 'oso-vertico-buffer--recount nil 'local)
  (setq oso--set-stack nil)
  (use-local-map (make-composed-keymap oso-completion-set-map (current-local-map)))
  (setq-local minibuffer-completion-predicate
              (let ((pred minibuffer-completion-predicate))
                (lambda (&rest str)
                  (and (or (not pred) (apply pred str))
                       (setq str (car str)
                             str (if (consp str) (car str) str)
                             str (if (symbolp str) (symbol-name str) str))
                       (or (not oso--current-set-predicate)
                           (not oso--narrow)
                           (funcall oso--current-set-predicate str))))))
  (setq-local oso-stack-ov
              (make-overlay (point-min) (point-min) nil t t))
  (overlay-put oso-stack-ov 'priority -100)
  (overlay-put oso-stack-ov 'window
               (overlay-get vertico--candidates-ov 'window)))
(advice-add 'vertico--setup :around 'oso--setup)

(defun oso--update-candidate-display ()
  (setq vertico--input t))

(cl-defgeneric oso--split ()
  (car (completion-boundaries (minibuffer-contents)
                              minibuffer-completion-table
                              minibuffer-completion-predicate
                              "")))

(cl-defmethod oso--split (&context ((car completion-styles)
                                    (eql consult--split)))
  (let* ((split (consult--async-split-style))
         (fn (plist-get split :function))
         (string (minibuffer-contents))
         (s (funcall fn string split)))
    (pcase s
      (`(,_ ,beg . ,_) beg)
      (_ (length string)))))

(defun oso-toggle-narrow ()
  (interactive)
  (unless (setq oso--narrow (not oso--narrow))
    (oso--update-candidate-display))
  (oso--update-stack))

(defun oso-pop (&optional clear)
  "Pop the head of the completion set stack and make it the
current completion set.

If CLEAR is non-nil clear the current completion set."
  (interactive "P")
  (cond (clear (oso--clear-stack))
        (oso--set-stack (pop oso--set-stack))
        (t (user-error "Too few elements in stack")))
  (oso--update-stack))

(defun oso-push (&optional complement)
  "Push current completion set to the stack.

If COMPLEMENT is non-nil push the complement of the current completion
set to the stack."
  (interactive "P")
  (let* ((beg (oso--split))
         (start (minibuffer--completion-prompt-end))
         (pat (substring (minibuffer-contents) beg)))
    (unless (string= pat "")
      (push (oso--build-pattern-predicate
             pat
             minibuffer-completion-table
             minibuffer-completion-predicate
             complement)
            oso--set-stack)
      (delete-region (+ start beg) (point-max))))
  (oso--update-stack))

(defun oso-complement ()
  "Complement the head of the completion set stack."
  (interactive)
  (oso-push)
  (when oso--set-stack
    (let* ((set (pop oso--set-stack))
           (memberp (oso-set-operation set)))
      (push (make-oso-set
             :operation (lambda (str)
                          (not (funcall memberp str)))
             :operands (list set)
             :description (concat "(∁ " (oso-set-description set) ")"))
            oso--set-stack))
    (oso--update-stack)))

(cl-defmacro oso-set-op (name symbol (str predicate1 predicate2) &body body)
  (declare (indent defun))
  (let ((set (gensym))
        (set1 (gensym))
        (set2 (gensym)))
    `(defun ,(intern (concat "oso-" (symbol-name name))) (&optional interactive)
       ,(if (stringp (car body)) (pop body) "")
       (interactive (list t))
       (when (called-interactively-p)
         (oso-push))
       (when (cdr oso--set-stack)
         (let* ((,set2 (pop oso--set-stack))
                (,set1 (pop oso--set-stack))
                (,predicate1 (oso-set-operation ,set1))
                (,predicate2 (oso-set-operation ,set2))
                (,set
                 (make-oso-set
                  :operation (lambda (,str) ,@body)
                  :operands (list ,set1 ,set2)
                  :description (concat "(" ,symbol " "
                                       (oso-set-description ,set1)
                                       " "
                                       (oso-set-description ,set2)
                                       ")"))))
           (push ,set oso--set-stack)))
       (oso--update-stack))))

(oso-set-op union "∪" (str p1 p2)
  "Union all completion sets."
  (or (funcall p1 str) (funcall p2 str)))

(oso-set-op intersection "∩" (str p1 p2)
  "Union all completion sets."
  (and (funcall p1 str) (funcall p2 str)))

(oso-set-op symmetric-difference "∆" (str p1 p2)
  "Union all completion sets."
  (xor (funcall p1 str) (funcall p2 str)))

(oso-set-op difference "∖" (str p1 p2)
  "Union all completion sets."
  (and (funcall p1 str) (not (funcall p2 str))))

(defun oso-swap ()
  "Union all completion sets."
  (interactive)
  (if (>= (length oso--set-stack) 2)
      (let* ((set1 (pop oso--set-stack))
             (set2 (pop oso--set-stack)))
        (push set1 oso--set-stack)
        (push set2 oso--set-stack))
    (user-error "Too few elements in stack"))
  (oso--update-stack))

(defun oso-reverse ()
  "Union all completion sets."
  (interactive)
  (setq oso--set-stack (reverse oso--set-stack))
  (oso--update-stack))

(defun oso-unwrap ()
  (interactive)
  (if-let ((set (pop oso--set-stack)))
      (pcase (oso-set-operands set)
        ('nil)
        ((and (pred listp) operands)
         (dolist (operand operands)
           (push operand oso--set-stack)))
        ((and (pred stringp) pattern)
         (delete-region (+ (minibuffer--completion-prompt-end)
                           (oso--split))
                        (point-max))
         (insert pattern)))
    (user-error "Nothing to unwrap."))
  (oso--update-stack))

(defun oso-reduce (command &optional right)
  (interactive
   (list (keymap-lookup nil (key-description
                             (read-key-sequence "Operation: ")))
         current-prefix-arg))
  (cond ((<= (length oso--set-stack) 1)
         (user-error "Too few elements in stack"))
        ((memq command '(oso-union
                         oso-intersection
                         oso-difference
                         oso-symmetric-difference))
         (when right (oso-reverse))
         (while (cdr oso--set-stack)
           (when right (oso-swap))
           (funcall command)))
        (t (user-error "Unknown operation.")))
  (oso--update-stack))

(defun oso--update-stack ()
  (setq oso--current-set-predicate
        (when oso--set-stack (oso-set-operation (car oso--set-stack))))
  (when oso--narrow
    (oso--update-candidate-display))
  (let ((inhibit-read-only t)
        (str ""))
    (mapcar (lambda (set)
              (setq str (concat str (oso-set-description set) "\n")))
            oso--set-stack)
    (add-face-text-property 0 (length str) 'oso-stack-face nil str)
    (when oso--narrow
      (setq str (concat str (propertize "[N]" 'face 'oso-narrow-indicator-face) " ")))
    (overlay-put oso-stack-ov 'before-string str)
    (when vertico-buffer-mode
      (setq-local vertico-count
                  (- (/ (window-pixel-height
                         (overlay-get oso-stack-ov 'window))
                        (default-line-height))
                     2
                     (with-temp-buffer
                       (insert str)
                       (count-matches "\n" (point-min) (point-max))))))))

(defun oso--category (cand)
  (let ((cat (vertico--metadata-get 'category)))
    (if (eq 'multi-category cat)
        (car (get-text-property 0 'multi-category cand))
      cat)))

(defun orderless-predicate-dispatch (predicates &rest args)
  (cl-loop for pred in predicates
           for result = (apply pred args)
           when (functionp result) do (cl-return result)))

(defun orderless-highlight-matches-ad (regexps strings)
  (when (stringp regexps)
    (setq regexps (car-safe (orderless-pattern-compiler regexps))))
  (let ((case-fold-search
         (if orderless-smart-case
             (cl-loop for regexp in regexps
                      always (isearch-no-upper-case-p regexp t))
           completion-ignore-case)))
    (cl-loop for original in strings
             for string = (copy-sequence original)
             collect (orderless--highlight regexps t string))))

(defun orderless-pattern-compiler-ad (pattern &optional styles dispatchers)
  (unless styles (setq styles orderless-matching-styles))
  (unless dispatchers (setq dispatchers orderless-style-dispatchers))
  (cl-loop
   with components = (if (functionp orderless-component-separator)
                         (funcall orderless-component-separator pattern)
                       (split-string pattern orderless-component-separator t))
   with total = (length components)
   with regexps = nil
   with preds = nil
   for component in components and index from 0 do
   (or (when-let ((fn (orderless-predicate-dispatch
                       (append orderless-predicate-dispatcher-overrides
                               orderless-predicate-dispatchers)
                       component index total)))
         (when (functionp fn) (push fn preds))
         t)
       (pcase-let ((`(,newstyles . ,newcomp)
                    (orderless-dispatch
                     dispatchers styles component index total)))
         (when (functionp newstyles)
           (setq newstyles (list newstyles)))
         (when-let ((res (cl-loop for style in newstyles
                                  for result = (funcall style newcomp)
                                  when result collect `(regexp ,result))))
           (push (rx-to-string `(or ,@(delete-dups res)))
                 regexps))))
   finally (return (cons (nreverse regexps)
                         (nreverse preds)))))

(defun orderless-filter-ad (string table &optional pred)
  "Split STRING into components and find entries TABLE matching all.
The predicate PRED is used to constrain the entries in TABLE."
  (save-match-data
    (pcase-let* ((`(,prefix . ,pattern)
                  (orderless--prefix+pattern string table pred))
                 (`(,completion-regexp-list . ,additional-predicates)
                  (orderless-pattern-compiler pattern))
                 (new-pred (if additional-predicates
                               (lambda (&rest args)
                                 (when (or (not pred) (apply pred args))
                                   (cl-every (lambda (fn) (apply fn args))
                                             additional-predicates)))
                             pred))
                 (completion-ignore-case
                  (if orderless-smart-case
                      (cl-loop for regexp in completion-regexp-list
                               always (isearch-no-upper-case-p regexp t))
                    completion-ignore-case)))
      ;; If there is a regexp of the form \(?:^quoted-regexp\) then
      ;; remove the first such and add the unquoted form to the prefix.
      (pcase (cl-some #'orderless--anchored-quoted-regexp
                      completion-regexp-list)
        (`(,regexp . ,literal)
         (setq prefix (concat prefix literal)
               completion-regexp-list (delete regexp completion-regexp-list))))
      (all-completions prefix table new-pred))))

(defun orderless-all-completions-ad (string table pred _point)
  "Split STRING into components and find entries TABLE matching all.
The predicate PRED is used to constrain the entries in TABLE.  The
matching portions of each candidate are highlighted.
This function is part of the `orderless' completion style."
  (let ((completions (orderless-filter string table pred)))
    (when completions
      (pcase-let ((`(,prefix . ,pattern)
                   (orderless--prefix+pattern string table pred)))
        (nconc (orderless-highlight-matches-ad pattern completions)
               (length prefix))))))

(defun orderless-annotation-pred (string _index _total)
  (when (string-match "\\`#\\(.+\\)" string)
    (let ((regexp (match-string 1 string)))
      (lambda (&rest args)
        (setq args (car args) ;; first argument is key
              args (if (consp args) (car args) args) ;; alist
              args (if (symbolp args) (symbol-name args) args)
              args (if (consult--tofu-p (aref args (1- (length args))))
                       (substring args 0 -1) args))
        (when-let* ((ann (vertico--metadata-get 'annotation-function))
                    (str (funcall ann args)))
          (string-match-p regexp str))))))

(defun orderless-contents-pred (string _index _total)
  (when (string-match "\\`@\\(.+\\)" string)
    (let ((regexp (match-string 1 string)))
      (lambda (&rest args)
        (setq args (car args) ;; first argument is key
              args (if (consp args) (car args) args) ;; alist
              args (if (symbolp args) (symbol-name args) args)
              args (if (consult--tofu-p (aref args (1- (length args))))
                       (substring args 0 -1) args))
        (pcase (oso--category args)
          ('file
           (or (f-directory-p args)
               (= 0 (call-process "rg" nil nil nil "-q" regexp
                                  (expand-file-name args)))))
          ('buffer
           (when-let ((buf (get-buffer args)))
             (with-current-buffer buf
               (save-excursion
                 (goto-char (point-min))
                 (save-match-data (re-search-forward regexp nil t))))))
          ('imenu
           (ignore-errors
             (when-let (cmd (intern-soft (seq-subseq args (1+ (seq-position args ? )))))
               (string-match-p regexp (documentation cmd)))))
          (_
           (ignore-errors
             (when-let (cmd (or (get-text-property 0 'embark-command args)))
               (string-match-p regexp (documentation cmd))))))))))

(defun orderless-major-mode-pred (string _index _total)
  (when (string-match "\\`\\*\\(.+\\)" string)
    (let ((mode (match-string 1 string)))
      (lambda (&rest args)
        (setq args (car args) ;; first argument is key
              args (if (consp args) (car args) args) ;; alist
              args (if (symbolp args) (symbol-name args) args)
              args (if (consult--tofu-p (aref args (1- (length args))))
                       (substring args 0 -1) args))
        (when-let ((buf (get-buffer args)))
          (with-current-buffer buf
            (or (string-match-p mode (symbol-name major-mode))
                (ignore-error (string-match-p mode (car-safe mode-name))))))))))

(defun orderless-buffer-modified-pred (string _index _total)
  (when (string-match "\\`\\*\\*" string)
    (lambda (&rest args)
      (setq args (car args) ;; first argument is key
            args (if (consp args) (car args) args) ;; alist
            args (if (symbolp args) (symbol-name args) args)
            args (if (consult--tofu-p (aref args (1- (length args))))
                     (substring args 0 -1) args))
      (when (eq 'buffer (oso--category args))
        (with-current-buffer args
          (and buffer-file-name
               (buffer-modified-p)))))))

(defmacro define-orderless-predicate-advice (name commands &rest predicates)
  (declare (indent 1))
  `(progn
     (defun ,name (fn &rest args)
       (let ((orderless-predicate-dispatcher-overrides ',predicates))
         (apply fn args)))
     ,@(cl-loop for cmd in (ensure-list commands)
                collect `(advice-add ',cmd :around ',name))))

;;;###autoload
(define-minor-mode orderless-predicate-mode
  "Orderless predicates"
  :global t
  :lighter ""
  (if orderless-predicate-mode
      (progn
        (advice-add 'orderless-highlight-matches :override 'orderless-highlight-matches-ad)
        (advice-add 'orderless-pattern-compiler :override 'orderless-pattern-compiler-ad)
        (advice-add 'orderless-filter :override 'orderless-filter-ad)
        (advice-add 'orderless-all-completions :override 'orderless-all-completions-ad))
    (advice-remove 'orderless-highlight-matches 'orderless-highlight-matches-ad)
    (advice-remove 'orderless-pattern-compiler 'orderless-pattern-compiler-ad)
    (advice-remove 'orderless-filter 'orderless-filter-ad)
    (advice-remove 'orderless-all-completions 'orderless-all-completions-ad)))

(provide 'orderless-set-operations)
